import npyscreen
import subprocess


class SetupApp(npyscreen.NPSAppManaged):
    def onStart(self):
        self.registerForm("MAIN", MainForm())


class MainForm(npyscreen.FormBaseNew):
    DEFAULT_LINES = 25
    DEFAULT_COLUMNS = 60
    SHOW_ATX = 10
    SHOW_ATY = 5

    def create(self):
        self.add(npyscreen.TitleFixedText, name="Setup | Web development virtual server", editable=False)
        self.lblStatus = self.add(npyscreen.TitleFixedText, name="Status", editable=False)

        self.add(npyscreen.FixedText, editable=False)
        self.add(npyscreen.FixedText, value="Machine controls", editable=False)
        self.btnStart = self.add(npyscreen.ButtonPress, name="Start", when_pressed_function=self.machine_start)
        self.btnReload = self.add(npyscreen.ButtonPress, name="Reload", when_pressed_function=self.machine_reload)
        self.btnStop = self.add(npyscreen.ButtonPress, name="Stop", when_pressed_function=self.machine_stop)

        self.add(npyscreen.FixedText, editable=False)
        self.add(npyscreen.FixedText, value="Server controls", editable=False)

        self.add(npyscreen.ButtonPress, name="Start LSWS", when_pressed_function=self.lsws_start)

        self.add(npyscreen.FixedText, editable=False)
        self.add(npyscreen.FixedText, value="Configuration", editable=False)

        self.add(npyscreen.ButtonPress, name="Provision", when_pressed_function=self.machine_provision)
        self.add(npyscreen.ButtonPress, name="Force provision w/ original config", when_pressed_function=self.machine_force_provision_with_config)
        self.add(npyscreen.ButtonPress, name="Rebuild machine", when_pressed_function=self.machine_rebuild)

        self.add(npyscreen.FixedText, editable=False)

        self.add(npyscreen.ButtonPress, name="Exit", when_pressed_function=self.exit)
        self.ready()

    def machine_start(self):
        self.status("Starting...")
        subprocess.run(["vagrant", "up"], capture_output=True)
        self.ready()

    def machine_reload(self):
        self.status("Reloading...")
        subprocess.run(["vagrant", "reload"], capture_output=True)
        self.ready()

    def machine_stop(self):
        self.status("Stopping...")
        subprocess.run(["vagrant", "halt", "-f"], capture_output=True)
        self.ready()

    def machine_provision(self):
        self.status("Running provision...")
        subprocess.run(["vagrant", "provision"], capture_output=True)
        self.ready()

    def machine_force_provision_with_config(self):
        self.status("Running provision w/ config...")
        subprocess.run(["vagrant", "ssh", "-c", "\"sudo rm -f /home/vagrant/.config-installed\""], capture_output=True)
        subprocess.run(["vagrant", "provision"], capture_output=True)
        self.ready()

    def machine_rebuild(self):
        self.status("Destroying machine...")
        subprocess.run(["vagrant", "destroy"], capture_output=True)
        self.status("Creating new machine...")
        subprocess.run(["vagrant", "up"], capture_output=True)
        self.ready()

    def lsws_start(self):
        self.status("Starting LSWS...")
        subprocess.run(["vagrant", "ssh", "-c", "\"sudo systemctl start lsws\""], capture_output=True)
        self.ready()

    def status(self, status):
        self.lblStatus.value = status
        self.display()

    def ready(self):
        self.lblStatus.value = "Ready..."
        self.display()

    def exit(self):
        self.parentApp.setNextForm(None)
        self.editing = False

if __name__ == '__main__':
    app = SetupApp()
    app.run()
