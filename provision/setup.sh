#!/usr/bin/sh
hostnamectl set-hostname server

# install apps/servers
rpm -ivh http://rpms.litespeedtech.com/centos/litespeed-repo-1.1-1.el7.noarch.rpm

yum -y install epel-release
yum -y install zip unzip php inotify-tools vim openlitespeed node mariadb mariadb-server wget
yum -y install lsphp73 lsphp73-json lsphp73-pecl-xdebug lsphp73-common lsphp73-mysqlnd lsphp73-process lsphp73-gd lsphp73-mbstring lsphp73-mcrypt lsphp73-bcmath lsphp73-pdo lsphp73-xml
yum -y install psmisc dos2unix

mkdir -p /srv/local

# add php73 to path and use it
echo 'export PATH=/usr/local/lsws/lsphp73/bin:$PATH' >> /home/vagrant/.bash_profile
source /home/vagrant/.bash_profile

# install composer globally
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer

# install phpMyAdmin
mkdir -p /srv/hosts/phpMyAdmin
chown nobody:nobody /srv/local
sudo -u nobody wget -q -P /srv/local https://files.phpmyadmin.net/phpMyAdmin/4.8.5/phpMyAdmin-4.8.5-all-languages.zip
sudo -u nobody unzip /srv/local/phpMyAdmin-4.8.5-all-languages.zip -d /srv/local
mv /srv/local/phpMyAdmin-4.8.5-all-languages /srv/local/phpMyAdmin/

# server folder
mkdir -p /srv/www
mkdir -p /srv/hosts
chown -R nobody:nobody /srv
chmod 755 /srv /srv/www

# insall config files
#  - runned only first time!
lock=/home/vagrant/.config-installed
if [ ! -e $lock ]; then
    /usr/local/lsws/bin/lswsctrl stop

    mkdir -p /usr/local/lsws/lsphp73/etc

    \cp -rf /home/vagrant/config/lsws/* /usr/local/lsws
    \cp -rf /home/vagrant/config/systemd/* /etc/systemd/system/
    \cp -rf /home/vagrant/config/php/* /usr/local/lsws/lsphp73/etc/php.d
    \cp /home/vagrant/config/phpMyAdmin/config.inc.php /srv/local/phpMyAdmin/config.inc.php

    chown nobody:nobody /srv/local/phpMyAdmin/config.inc.php
    chown -R lsadm:lsadm /usr/local/lsws/conf
    chown -R lsadm:lsadm /usr/local/lsws/admin/conf
    chown -R root:root /usr/local/lsws/lsphp73/etc/php.d

    touch $lock
fi

# enable and start servers
systemctl enable lsws
systemctl start lsws

systemctl enable mariadb
systemctl start mariadb

# automatic vhost directory
\cp -f /vagrant/scripts/monitor.sh /srv/monitor.sh
dos2unix /srv/monitor.sh
chown nobody:nobody /srv/monitor.sh
chmod +x /srv/monitor.sh

systemctl enable vhost-monitor.timer
systemctl start vhost-monitor.timer
