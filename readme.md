# Web development virtual server

Simple development vagrant virtual machine based on CentOS and OpenLiteSpeed. Provides host-like speed development and two-way file share via NFS.

## Features
 - http server provided by OpenLiteSpeed with PHP 7.3 via LSAPI
 - mysql server provided by MariaDB
 - preinstalled phpMyAdmin, composer
 - fast two-way file share via nfs
 - npyscreen UI for machine management

## Requirements
 - vagrant `vagrant-vbguest` plugin
 - windows hosts **MUST** install `vagrant-winnfsd` plugin for nfs to work
 - VirtualBox
 - Vagrant
 - dnsmasq or acrylic (Windows) - optional but prefered
 - Python 3.5+ with PIP - requirements file for windows are provided in windows.txt

## Usage
The repository now contains a simple setup python script. You can use it to configure and run your instance or use vagrant directly. To start the tool use `python setup.py`.

Clone or download the repository. Update Vagrantfile to use your desired development folder for syncing. The vhosts root in the guest machine is in `/srv/www`. Your WebDevelopment root folder (folder that contains folders with name of the host => `.../some-development-folder/{host-name-without-tld}/...`) should be synced there.

Server is by default configured to work on tld `.d`. That's because the tld `.dev` has been registered as valid tld and chrome and firefox decided to require SSL by HSTS. Simply, you would need to provide certificates for your development websites.

For automatic domain discovery, you'll need to configure dnsmasq or acrylic to redirect `*.d` to `192.168.2.10`.

The OpenLiteSpeed administration is present at `server.d:7080` with username `admin` and password `123456`. You can register new VHosts there. There are two presets for vhosts. The only difference between them is that `Development` will use the `/srv/www/{vhost-name}` directory as document root and `DevelopmentPublic` will use `/srv/www/{vhost-name}/public` as document root. This differentiation is due to OpenLiteSpeed's limitations, so that you don't have to edit the configuration for every vhost by hand. VHosts needs to be registered to work.

OpenLiteSpeed also needs one more directory for each vhost - vhost root. Theese folders are automatically generated in `/srv/hosts/{vhost-name}`. There is a timer script which watches `/srv/www` directory and automatically creates the vhost root directory. The timer runs every 5 seconds for better performance - sometimes you'll have to wait for the folder to create.

Login for phpMyAdmin/MariaDB server is `root`, without password.